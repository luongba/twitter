import { NextFunction, Request, Response } from 'express'
import usersService from '~/services/users.services'
import { ParamsDictionary } from 'express-serve-static-core'
import { validationResult } from 'express-validator'
import { RegisterReqBody, LoginReqBody, TokenPayload } from '~/models/schemas/requests/Users.requests'
import { ObjectId } from 'mongodb'
import User from '~/models/schemas/User.schema'
import databaseService from '~/services/database.services'
import HTTP_STATUS from '~/constants/httpStatus'
import { USER_MESSAGES } from '~/constants/messages'
import { UserVerifyStatus } from '~/constants/enums'
import { ErrorWithStatus } from '~/models/Errors'
export const loginController = async (
  req: Request<ParamsDictionary, any, LoginReqBody>,
  res: Response,
  next: NextFunction
) => {
  const user = req.user as User
  const user_id = user._id as ObjectId
  const result = await usersService.login(user_id.toString())
  return res.json({
    message: 'Login success',
    result
  })
}

export const registerController = async (
  req: Request<ParamsDictionary, any, RegisterReqBody>,
  res: Response,
  next: NextFunction
) => {
  const result = await usersService.register(req.body)

  return res.json({
    message: 'Register success',
    result
  })
}
export const logoutController = async (req: Request, res: Response, next: NextFunction) => {
  const { refresh_token } = req.body
  const result = await usersService.logout(refresh_token)
  res.json({
    message: 'Logout success',
    result
  })
}

export const emailVerifyController = async (req: any, res: Response, next: NextFunction) => {
  const { user_id } = req.decode_email_verify_token as TokenPayload
  const user = await databaseService.users.findOne({
    _id: new ObjectId(user_id)
  })
  if (!user) {
    return res.status(HTTP_STATUS.NOT_FOUND).json({
      message: USER_MESSAGES.USER_NOT_FOUND
    })
  }
  if (user.email_verify_token === '') {
    return res.status(HTTP_STATUS.OK).json({
      message: USER_MESSAGES.USER_VERIFIED
    })
  }
  const verifyEmail = await usersService.verifyEmail(user_id)
  return res.json({
    message: 'verify email success',
    verifyEmail
  })
}
export const resendemailVerifyController = async (req: any, res: Response, next: NextFunction) => {
  const { user_id } = req.decode_authorization as TokenPayload
  const user = await databaseService.users.findOne({
    _id: new ObjectId(user_id)
  })
  if (!user) {
    return res.status(HTTP_STATUS.NOT_FOUND).json({
      message: USER_MESSAGES.USER_NOT_FOUND
    })
  }
  if (user.verify === UserVerifyStatus.Verified) {
    return res.status(HTTP_STATUS.OK).json({
      message: USER_MESSAGES.USER_VERIFIED
    })
  }
  const verifyEmail = await usersService.resendVerifyEmail(user_id)
  return res.json({
    ...verifyEmail
  })
}
export const forgotPasswordController = async (req: any, res: Response, next: NextFunction) => {
  const { _id } = req.user
  const result = await usersService.forgotPassword(_id.toString())
  res.json({
    message: 'Send email take token successfully',
    forgot_password_token: result
  })
}
export const resetPasswordController = async (req: any, res: Response, next: NextFunction) => {
  const { user_id } = req.decode_forgot_password_token as TokenPayload
  const { password } = req.body
  const result = await usersService.resetPassword(user_id, password)
  if (result) {
    res.json({
      message: 'Send email take token successfully',
      result
    })
  }
}
