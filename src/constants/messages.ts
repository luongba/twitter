export const USER_MESSAGES = {
  VALIDATION_ERROR: 'Validation error',
  NAME_NOTEMPTY: 'Name cannot be empty',
  NAME_STRING: 'Name must be a string',
  NAME_LENGTH: 'Name must be between 2 and 100 characters in length',
  NAME_TRIM: 'Name will be trimmed',
  EMAIL_NOTEMPTY: 'Email cannot be empty',
  EMAIL_EMAIL: 'Email is not valid',
  EMAIL_TRIM: 'Email will be trimmed',
  EMAIL_CUSTOM: 'Email already exists',
  PASSWORD_NOTEMPTY: 'Password cannot be empty',
  PASSWORD_STRING: 'Password must be a string',
  PASSWORD_LENGTH: 'Password must be between 6 and 50 characters in length',
  PASSWORD_STRONG: 'Password must be strong',
  PASSWORD_CONFIRM_NOTEMPTY: 'Password confirm cannot be empty',
  PASSWORD_CONFIRM_STRING: 'Password confirm must be a string',
  PASSWORD_CONFIRM_LENGTH: 'Password confirm must be between 6 and 50 characters in length',
  PASSWORD_CONFIRM_STRONG: 'Password confirm must be strong',
  DATE_FORMAT: 'Date of birth must be in ISO8601 format',
  PASSWORD_MATCH: 'Password confirmation does not match password',
  EMAIL_NOT_EXIST: 'Email does not exist in the system',
  EMAIL_PASSWORD_NOT_CORRECT: 'Email or password is incorrect',
  ACCESS_TOKEN_IS_REQUIRED: 'Access token cannot be empty',
  REFRESH_TOKEN_IS_REQUIRED: 'Refresh token cannot be empty',
  REFRESH_TOKEN_IS_INVALID: 'Refresh token is invalid',
  FORGOT_PASSWORD_TOKEN_IS_REQUIRED: 'Forgot password token cannot be empty',
  FORGOT_PASSWORD_TOKEN_IS_INVALID: 'Forgot password token is invalid',
  USED_REFRESH_TOKEN_OR_NOT_EXIST: 'Refresh token does not exist',
  EMAIL_VERIFY_TOKEN_IS_REQUIRED: 'Email Verify token cannot be empty',
  USER_NOT_FOUND: 'User not found',
  USER_VERIFIED: 'User verified',
  EMAIL_NOT_EXISTS: 'Email does not exists',
  REQUEST_NOT_INVALID: 'Invalid request'
} as const
