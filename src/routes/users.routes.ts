import { Router } from 'express'
import {
  loginValidator,
  accessTokenValidator,
  registerValidator,
  refreshTokenValidator,
  verifyEmailTokenValidator,
  forgotPasswordValidator,
  resetPasswordValidator
} from '~/middlewares/users.middlewares'
import {
  emailVerifyController,
  forgotPasswordController,
  loginController,
  logoutController,
  registerController,
  resendemailVerifyController,
  resetPasswordController
} from '~/controllers/users.controllers'
import { wrapRequestHandle } from '~/utils/handlers'
const router = Router()
/**
 * Description: user Login
 * Path: /login
 * Method: POST
 * Body: { email: string, password: string }
 */
router.post('/login', loginValidator, loginController)
/**
 * Description: register a new user
 * Path: /register
 * Method: POST
 * Body: { name: string, email: string, password: string, confirm_password: string date_of_birth: ISO8601 }
 */
router.post('/register', registerValidator, wrapRequestHandle(registerController))
/**
 * Description: register a new user
 * Path: /logout
 * Method: POST
 * Body: { accessToken: string, refresh_token: string }
 */
router.post('/logout', accessTokenValidator, refreshTokenValidator, wrapRequestHandle(logoutController))
/**
 * Description: verify email
 * Path: /verify-email
 * Method: POST
 * Body: { emai_verify_token: string }
 */
router.post('/verify-email', verifyEmailTokenValidator, wrapRequestHandle(emailVerifyController))
/**
 * Description: resend verify email
 * Path: /resend-verify-email
 * Method: POST
 * Header: { Authorization: { access_token: string } }
 */
router.post(
  '/resend-verify-email',
  accessTokenValidator,
  wrapRequestHandle(resendemailVerifyController)
)

/**
 * Description: forgot password
 * Path: /forgot-password
 * Method: POST
 * body: { email: string }
 */
router.post(
  '/forgot-password',
  forgotPasswordValidator,
  wrapRequestHandle(forgotPasswordController)
)
/**
 * Description: reset password
 * Path: /reset-password
 * Method: POST
 * Header: { Authorization: { forgot_password_token: string } }
 * body: { password, confirm_password }
 */
router.post(
  '/reset-password',
  resetPasswordValidator,
  wrapRequestHandle(resetPasswordController)
)


export default router
